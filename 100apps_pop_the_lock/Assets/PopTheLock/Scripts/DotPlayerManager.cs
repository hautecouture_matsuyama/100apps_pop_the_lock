﻿using UnityEngine;
using System.Collections;

public class DotPlayerManager : MonoBehaviour {

	[SerializeField] RectTransform _playerTransform;
	[SerializeField] RectTransform _dotTransform;

	RectTransform rectTransform{get{return (RectTransform)transform;}}

	public void UpdateRotations(){
		var startPlayerRotation = _playerTransform.rotation;
		var dotRot = _dotTransform.rotation;
		rectTransform.rotation = startPlayerRotation;
		_playerTransform.localRotation = Quaternion.identity;
		_dotTransform.rotation = dotRot;
	}

	public float eulerDifference{get{ return _playerTransform.localEulerAngles.z-_dotTransform.localEulerAngles.z;}}

	public float GetDifference(int direction){
		return eulerDifference*-direction;
	}

	public bool GetIsAfter(int direction){
		//Debug.Log (GetDifference(direction));
		return GetDifference (direction) < 0;
	}

	public float eulerDiffenceAbs{get{return Mathf.Abs (eulerDifference);}}

}
