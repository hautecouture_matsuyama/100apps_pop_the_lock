﻿using UnityEngine;
using System.Collections;
using System;

namespace AppAdvisory.Ads
{
	public class AdBase : MonoBehaviour
	{
		public ADIDS adIds
		{
			get
			{
				return FindObjectOfType<AppAdvisory.Ads.AdsManager>().adIds;
			}
		}
	}
}