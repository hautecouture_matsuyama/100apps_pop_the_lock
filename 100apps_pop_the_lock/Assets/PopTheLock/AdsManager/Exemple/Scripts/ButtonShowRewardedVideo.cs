﻿using UnityEngine;
using System.Collections;
using AppAdvisory.Ads;

public class ButtonShowRewardedVideo : MonoBehaviour 
{
	public void OnClicked()
	{
		FindObjectOfType<AppAdvisory.Ads.AdsManager>().ShowRewardedVideo(delegate(bool obj) {
			print("rewarded video success ? ===> " + obj);
		});
	}
}
