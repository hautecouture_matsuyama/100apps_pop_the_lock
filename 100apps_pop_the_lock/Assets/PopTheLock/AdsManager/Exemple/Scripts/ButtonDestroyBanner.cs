﻿using UnityEngine;
using System.Collections;
using AppAdvisory.Ads;

public class ButtonDestroyBanner : MonoBehaviour 
{
	public void OnClicked()
	{
		FindObjectOfType<AppAdvisory.Ads.AdsManager>().DestroyBanner();
	}
}
