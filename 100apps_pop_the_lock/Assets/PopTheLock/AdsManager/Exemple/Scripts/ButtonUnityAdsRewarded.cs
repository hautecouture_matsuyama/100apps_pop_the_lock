﻿using UnityEngine;
using System.Collections;

public class ButtonRewardedVideo : MonoBehaviour 
{
	void Start () 
	{
		#if UNITY_ADS
		GetComponentInChildren<UnityEngine.UI.Text>().text = "Rewarded video";
		#else
		gameObject.SetActive(false);
		#endif
	}

	public void ShowRewardedVideo()
	{
		#if UNITY_ADS
		FindObjectOfType<AppAdvisory.Ads.AdsManager>().ShowRewardedVideo((bool success) => {
			Debug.Log("rewarded video : success? " + success);
		});
		#endif
	}
}
