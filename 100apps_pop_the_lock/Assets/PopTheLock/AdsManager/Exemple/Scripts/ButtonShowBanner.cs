﻿using UnityEngine;
using System.Collections;
using AppAdvisory.Ads;

public class ButtonShowBanner : MonoBehaviour 
{
	public void OnClicked()
	{
		FindObjectOfType<AppAdvisory.Ads.AdsManager>().ShowBanner();
	}
}
