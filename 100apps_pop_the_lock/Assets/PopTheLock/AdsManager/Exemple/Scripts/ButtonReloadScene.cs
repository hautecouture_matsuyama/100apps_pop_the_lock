﻿using UnityEngine;
using System.Collections;
#if UNITY_5_3
using UnityEngine.SceneManagement;
#endif

public class ButtonReloadScene : MonoBehaviour 
{
	void Start()
	{
		GetComponentInChildren<UnityEngine.UI.Text>().text = "Reload Scene";
	}

	public void OnClicked()
	{
		#if UNITY_5_3
		SceneManager.LoadSceneAsync(0,LoadSceneMode.Single);
		#else
		Application.LoadLevelAsync(0);
		#endif
	}
}
