﻿using UnityEngine;
using System.Collections;

public class ButtonAdmobInterstitial : MonoBehaviour
{
	void Start () 
	{
		#if GOOGLE_MOBILE_ADS
		GetComponentInChildren<UnityEngine.UI.Text>().text = "ADColony rewarded video";
		#else
		gameObject.SetActive(false);
		#endif
	}
}
