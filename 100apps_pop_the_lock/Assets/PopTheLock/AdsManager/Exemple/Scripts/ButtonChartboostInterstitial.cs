﻿using UnityEngine;
using System.Collections;

public class ButtonChartboostInterstitial : MonoBehaviour 
{
	void Start () 
	{
		#if CHARTBOOST
		GetComponentInChildren<UnityEngine.UI.Text>().text = "Chartboost interstitial";
		#else
		gameObject.SetActive(false);
		#endif
	}
}
