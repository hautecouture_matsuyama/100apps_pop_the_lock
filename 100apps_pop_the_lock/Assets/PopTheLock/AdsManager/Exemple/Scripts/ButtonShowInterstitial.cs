﻿using UnityEngine;
using System.Collections;
using AppAdvisory.Ads;

public class ButtonShowInterstitial : MonoBehaviour 
{
	public void OnClicked()
	{
		FindObjectOfType<AppAdvisory.Ads.AdsManager>().ShowInterstitial();
	}
}
