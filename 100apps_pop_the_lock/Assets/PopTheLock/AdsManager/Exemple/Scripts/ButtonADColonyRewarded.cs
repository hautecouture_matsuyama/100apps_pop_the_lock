﻿using UnityEngine;
using System.Collections;

public class ButtonADColonyRewarded : MonoBehaviour
{
	void Start () 
	{
		#if ADCOLONY
		GetComponentInChildren<UnityEngine.UI.Text>().text = "ADColony rewarded video";
		#else
		gameObject.SetActive(false);
		#endif
	}
}
