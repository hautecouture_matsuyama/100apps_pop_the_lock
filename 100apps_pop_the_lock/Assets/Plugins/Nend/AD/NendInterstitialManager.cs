﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

using NendUnityPlugin.AD;
using NendUnityPlugin.Common;

/// <summary>
/// インタースティシャルの表示準備、自動表示用のマネージャー.
/// オブジェクトにアタッチしておけば少なくとも表示準備までは済ませる.
/// </summary>
public class NendInterstitialManager : MonoBehaviour
{

    #region DefineAccountSettings
    public class Platform
    {
        public string ApiKey = "";
        public int SpotID = 0;
    }

    [System.Serializable]
    public class Android : Platform { }
    [System.Serializable]
    public class IOS : Platform { }
    
    [System.Serializable]
    public class Account
    {
        [SerializeField]
        public Android android;
        [SerializeField]
        public IOS iOS;
    }
    [SerializeField]
    private Account account;
    #endregion

    [SerializeField]
    private float reloadIntervalSec = 2f;

    [SerializeField]
    private float reloadRetryCount = 2;

    private float curRetryCount = 0;

    [SerializeField]
    private bool automaticDisplay;

    private bool displayed;

    [SerializeField]
    private UnityEvent onAdClickedEvent = new UnityEvent();
    [SerializeField]
    private UnityEvent onAdCancelledEvent = new UnityEvent();

    public static NendInterstitialManager Instance
    {
        get
        {
            return instance;
        }
    }

    static NendInterstitialManager instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    bool cantConnectOnStart;
    bool clickEventRegistered;

    bool initializedOnStart;

    void OnEnable()
    {
        if (initializedOnStart)
        {
            Initialize();
        }
    }

    void Start()
    {
        if(account.android.ApiKey == "" && account.iOS.ApiKey == "")
        {
            Debug.LogError("APIキーを設定してください.このままではアプリ起動時にクラッシュします.");
            return;
        }

        if(Application.internetReachability == NetworkReachability.NotReachable)
        {
            cantConnectOnStart = true;
            return;
        }

        Initialize();
        initializedOnStart = true;
    }

    void Initialize()
    {
        // attach EventHandler
        NendAdInterstitial.Instance.AdLoaded += OnFinishLoadInterstitialAd;
        NendAdInterstitial.Instance.AdShown += OnShownInterstitialAd;

        // 表示した広告をクリック
        if(onAdClickedEvent.GetPersistentEventCount() != 0 ||
            onAdCancelledEvent.GetPersistentEventCount() != 0) {
            NendAdInterstitial.Instance.AdClicked += OnClickInterstitialAd;
            clickEventRegistered = true;
        }

        StartCoroutine(LoadAd());
    }

    void Update()
    {
        if (!cantConnectOnStart) return;
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            cantConnectOnStart = false;
            Initialize();
        }
    }

    void OnDestroy()
    {
        // detach EventHandler
        NendAdInterstitial.Instance.AdLoaded -= OnFinishLoadInterstitialAd;
        NendAdInterstitial.Instance.AdShown -= OnShownInterstitialAd;

        if (clickEventRegistered)
        {
            NendAdInterstitial.Instance.AdClicked -= OnClickInterstitialAd;
        }
    }

    private IEnumerator LoadAd()
    {
        string apiKey = "";
        string spotId = "";

#if UNITY_IPHONE
		apiKey = account.iOS.ApiKey;
		spotId = account.iOS.SpotID.ToString();
		Handheld.SetActivityIndicatorStyle(iOSActivityIndicatorStyle.Gray);
#elif UNITY_ANDROID
        apiKey = account.android.ApiKey;
        spotId = account.android.SpotID.ToString();
        Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Small);
#endif

        Handheld.StartActivityIndicator();
        yield return new WaitForSeconds(0.0f);

        NendAdInterstitial.Instance.Load(apiKey, spotId);
    }

    private void ReloadAd()
    {
        StartCoroutine(LoadAdDelay());
    }

    private IEnumerator LoadAdDelay()
    {
        yield return new WaitForSeconds(reloadIntervalSec);
        StartCoroutine(LoadAd());
    }

    public void OnFinishLoadInterstitialAd(object sender, NendAdInterstitialLoadEventArgs args)
    {
        Handheld.StopActivityIndicator();

        NendAdInterstitialStatusCode statusCode = args.StatusCode;
        switch (statusCode)
        {
            case NendAdInterstitialStatusCode.SUCCESS:
                if (automaticDisplay && !displayed)
                {
                    displayed = true;
                    NendAdInterstitial.Instance.Show();
                }
                break;
            default:
                if (curRetryCount++ <= reloadRetryCount)
                {
                    ReloadAd();
                }
                break;
        }
    }

    public void OnShownInterstitialAd(object sender, NendAdInterstitialShowEventArgs args) {
        curRetryCount = 0;
        StartCoroutine(LoadAd());
    }

    void OnClickInterstitialAd(object sender, NendAdInterstitialClickEventArgs args)
    {
        switch (args.ClickType)
        {
            case NendAdInterstitialClickType.DOWNLOAD:
                onAdClickedEvent.Invoke();
                break;
            case NendAdInterstitialClickType.CLOSE:
            case NendAdInterstitialClickType.EXIT:
                onAdCancelledEvent.Invoke();
                break;
        }
    }
}
